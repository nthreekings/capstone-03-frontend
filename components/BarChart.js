import React, { useState, useEffect } from 'react';
import { Form, Row, Col, Card } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

export default function BarChart({records}) {

	const [categoryType, setCategoryType] = useState([]);
	const [categoryAmount, setCategoryAmount] = useState([]);
    const [transactions, setTransactions] = useState([]);
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');

    // get category types
    useEffect(() => {

    	let type = [];

    	records.forEach((record) => {

    		if(type.indexOf(record.categoryType) === -1) {

    			type.push(record.categoryType)

    		}

    	})

    	setCategoryType(type)

    },[records])

    // pre-load expense records
    useEffect(() => {

        setTransactions(categoryType)

    },[categoryType])

    // get filtered transaction
    useEffect(() => {

        let filteredTransaction = []

        records.forEach((record) => {

            if(startDate !== '' || endDate !== '') {

                 if(moment(record.dateAdded).format('yyyy-MM-DD') >= moment(startDate).format('yyyy-MM-DD') && moment(record.dateAdded).format('yyyy-MM-DD') <= moment(endDate).format('yyyy-MM-DD')) {

                    filteredTransaction.push(record.amount)

                }

            }   

        })

        setTransactions(filteredTransaction)

    },[startDate, endDate])

    //  get total amount of each category type 
    useEffect(() => {

        setCategoryAmount(categoryType.map((category) => {

            let amount = 0;

            records.filter((record) => {

                if (record.categoryType === category) {

                    amount += record.amount

                }

            })

            return amount;

        }))

    }, [categoryType])

    const options = {
        maintainAspectRatio: false,
        scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
        },
    };


    /*
    const colors = ["#6953F7","#CD4FF7","#F6AA65","#089450","#FB3BA1","#AA9EFA","#DE8AF9","#302D43","#C5D6D8","#CB958E","#99F7AB","#7E78D2","#8E9DCC","#7D84B2","#9EEFE5","#4F7CAC","#ABDF75","#C0E0DE","#F3B3A6","#CEFDFF","#B98B82","#D6F9DD","#E4959E","#C5D6D8","#CB958E","#99F7AB","#7E78D2"]

    //console.log(categories)
    */
    return (
        <React.Fragment>
            <Card className="card-component chart-component my-4">
                <Card.Body>
                    <Card.Title>
                        <h3 className="text-white">Income vs Expense</h3>
                        <Form.Group>
                            <Row>
                                <Col xs md="6" className="my-1 pr-2">
                                    {/*<Form.Label className="text-muted"> From </Form.Label>*/}
                                    <Form.Control
                                        className="input"
                                        type="date"
                                        value={moment(startDate).format('yyyy-MM-DD')}
                                        onChange={e => setStartDate(e.target.value)}
                                    />
                                </Col>

                                <Col xs md="6" className="my-1 pr-2">
                                    {/*<Form.Label className="text-muted"> To </Form.Label>*/}
                                    <Form.Control
                                        className="input"
                                        type="date"
                                        value={moment(endDate).format('yyyy-MM-DD')}
                                        onChange={e => setEndDate(e.target.value)}
                                    />
                                </Col>
                            </Row>  
                        </Form.Group>
                    </Card.Title>
                    {(categoryAmount.length === 0)
                    ?   <h5 className="text-muted text-center mt-2">No transactions yet</h5>
                    :   <Bar
                            options={options}

                            data = {{
                                labels: categoryType,
                                datasets: [{

                                    label: 'Income vs Expense',
                                    data: categoryAmount,
                                    backgroundColor: ['rgba(54, 162, 235, 0.2)', 'rgba(153, 102, 255, 0.2)'],
                                    borderColor: ['rgb(54, 162, 235)', 'rgb(153, 102, 255)'],
                                    borderWidth: 1,
                                    hoverBackgroundColor: ['rgba(54, 162, 235, 0.2)', 'rgba(153, 102, 255, 0.2)'],
                                    hoverBorderColor: ['rgb(54, 162, 235)', 'rgb(153, 102, 255)'],
                                    
                                }]
                            }}

                            redraw={false}
                        />
                    }
                    
                </Card.Body>
            </Card>
        </React.Fragment>
    )
}