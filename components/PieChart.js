import { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';

export default function PieChart({records}) {

    const [categories, setCategories] = useState([]);
    const [categoryAmount, setCategoryAmount] = useState([]);

    useEffect(() => {

        let categories = [];

        records.forEach((record) => {

            if(categories.indexOf(record.categoryName) === -1) {

                categories.push(record.categoryName)

            }

        })

        setCategories(categories)

    },[records])

    useEffect(() => {

        setCategoryAmount(categories.map((category) => {

            let amount = 0;

            records.forEach((record) => {

                if (record.categoryName === category) {

                    amount -= record.amount

                }

            })

            return amount;

        }))

    }, [categories])

    const options = {
      maintainAspectRatio: false,
      responsive: false,
      legend: {
        position: 'left',
        labels: {
          boxWidth: 10
        }
      }
    }


    const colors = ["#6953F7","#CD4FF7","#F6AA65","#089450","#FB3BA1","#AA9EFA","#DE8AF9","#302D43","#C5D6D8","#CB958E","#99F7AB","#7E78D2","#8E9DCC","#7D84B2","#9EEFE5","#4F7CAC","#ABDF75","#C0E0DE","#F3B3A6","#CEFDFF","#B98B82","#D6F9DD","#E4959E","#C5D6D8","#CB958E","#99F7AB","#7E78D2"]

    //console.log(categories)

    return (
        <Pie
            options={options}
            data = {{
                labels: categories,
                datasets: [

                    { 
                        data: categoryAmount, 
                        backgroundColor: colors,
                        hoverBackgroundColor: colors,
                        borderWidth: 0
                    }
                
                ]
            }}
            redraw={false}
        />
    )

}