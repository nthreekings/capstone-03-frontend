import { Container, Spinner, Jumbotron } from 'react-bootstrap';

export default function Loading(){
	return (
		<Container className="d-flex justify-content-center h-100">
			<div className="d-flex align-items-center">
				<Spinner className="spinner" animation="border" role="status" >
				  <span className="sr-only text-white">Loading...</span>
				</Spinner>
			</div>
		</Container>
	)
}