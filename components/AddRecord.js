import { useState, useEffect, useContext } from 'react';
import { Row, Col, Card, Form, Button } from 'react-bootstrap';
import AddCategory from './AddCategory';
import Swal from 'sweetalert2';
import AppHelper from '../app-helper';
import UserContext from '../UserContext';

export default function AddRecord({ setRecords }) {

	const { user } = useContext(UserContext);
	const [categories, setCategories] = useState([]);
	const [categoryId, setCategoryId] = useState('');
	const [categoryName, setCategoryName] = useState('');
	const [categoryType, setCategoryType] = useState('');
	const [amount, setAmount] = useState('');
	const [description, setDescription] = useState('');
	const [transactionCategories, setTransactionCategories] = useState([]);
	const [isActive, setIsActive] = useState(false);

    // fetch user records and categories hook
    useEffect(() => {
        fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/details`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {

            setCategories(data.categories)

        })
    },[])

	// pre-load category name and type once category prop gets passed hook
	useEffect(() => {

		(categories.length === 0)
		? setCategoryName('')
		: setCategoryName(categories[0].categoryName)

	}, [categories])

	// verify record amount
	useEffect(()=> {

		((amount.length > 0 && isNaN(amount) === false) && (categoryType.length > 0 && categoryName.length > 0))
		? setIsActive(true)
		: setIsActive(false)

	}, [amount, categoryName, categoryType])

	// auto-set category ID after choosing category name
	useEffect(() => {

		const autoId = categories.find(autoId => autoId.categoryName === categoryName)

		return(

			(autoId === undefined)
			? setCategoryId('')
			: setCategoryId(autoId._id)

		)

	},[categoryName])

	// set category name options based on category type
	useEffect(() => {

		let categoryOptions =  categories.map(category => {

			if (category.categoryType === categoryType) {

				return(
					<option key={category._id}>{category.categoryName}</option>
				)

			} else {

				return null

			}
			
		})

		setTransactionCategories(categoryOptions);

	}, [categoryType])

	function addRecord(e) {

		e.preventDefault(e)

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${user.id}/transactions`, {

			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				categoryId: categoryId,
				categoryName: categoryName,
				categoryType: categoryType,
				amount: amount,
				description: description
			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			data
			? fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
				headers: {
					'Authorization' : `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(AppHelper.toJSON)
			.then(data => {
				setRecords(data.transactions)
				setDescription('')
				setAmount('')
				Swal.fire({
					title: 'Added Record!',
					text: 'Record is added successfully',
					icon: 'success',
                	background: '#121212',
                	backdrop: 'rgba(30, 30, 30, 0.9)'
				})
				.then((result) => {
					// window.location.replace("../transactions")
					result.dismiss === Swal.DismissReason.timer
                        ? null
                        : null
				})
			})
		: 	Swal.fire({
				title: 'Uh oh!',
				text: 'Something is wrong',
				icon: 'error',
            	background: '#121212',
            	backdrop: 'rgba(30, 30, 30, 0.9)'
			})
			.then((result) => {
				result.dismiss === Swal.DismissReason.timer
                        ? null
                        : null
			})
		})
	}

	return(
		<Card className="card-component my-4">
			<Card.Body>
				<Card.Title><h3 className="text-white">Add Transaction</h3></Card.Title>
				<Form onSubmit={(e) => addRecord(e)}>
					<Form.Row>
						
						<Col md="6">
							<Form.Label className="text-muted mt-2">Transaction Type</Form.Label>
							<Form.Control
								className="input"
								as="select"
								value={categoryType}
								onChange={(e) => setCategoryType(e.target.value)}
							
								required
							>
								<option>Select category..</option>
								<option value="Expense">Expense</option>
								<option value="Income">Income</option>
							</Form.Control>
						</Col>

						<Col md="6">
							<Form.Label className="text-muted mt-2 d-flex justify-content-between">Transaction Category
								<AddCategory setCategories={setCategories} />
							</Form.Label>
							<Form.Control 
								className="input"
								as="select"
								value={categoryName}
								onChange={(e) => setCategoryName(e.target.value)}
								required 
							>	
								
								<option>Select category..</option>
								{transactionCategories}
								
							</Form.Control>
						</Col>

						<Col md="6">
							<Form.Label className="text-muted mt-2">Description</Form.Label>
							<Form.Control 
								className="input"
								type="text"
								placeholder="Description"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
								required 
							/>
						</Col>
						
						<Col md="6">
							<Form.Label className="text-muted mt-2">Amount</Form.Label>
							<Form.Control
								className="input"
								type="text"
								value={amount}
								placeholder="0"
								onChange={(e) => setAmount(e.target.value)}
								required
							/>
						</Col>
						
						{ isActive === true 
							? <Button type="submit" block variant="success" className="btn-purple mt-3"><span className="d-flex align-self-center justify-content-center">+</span>
							  </Button>
		            		: <Button type="submit" block variant="outline-success" className="btn-purple-outline mt-3" disabled><span className="d-flex align-self-center justify-content-center">+</span></Button>
						}

					</Form.Row>
				</Form>
			</Card.Body>
		</Card>
	)
}