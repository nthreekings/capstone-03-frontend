import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Modal, Col, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import AppHelper from '../app-helper';
import UserContext from '../UserContext';

export default function AddCategory({ setCategories }) {

	const { user } = useContext(UserContext);
	const [categoryName, setCategoryName] = useState('');
	const [categoryType, setCategoryType] = useState('Expense');
	const [isActive, setIsActive] = useState(false);
    const [ show, setShow ] = useState(false);
    const showModal = () => setShow(true);

    // reset update form upon modal close
    const closeModal = () => {
        setShow(false)
        setCategoryName('')
        setCategoryType('')
    }

	useEffect(() => {

		(categoryName.length < 30 && categoryName.length > 0)
		? setIsActive(true)
		: setIsActive(false)

	}, [categoryName])

	function addCategory(e) {

		e.preventDefault()

		fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/${user.id}/categories`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				categoryName: categoryName,
				categoryType: categoryType
			})
		})
		.then(AppHelper.toJSON)
		.then(data => {

			data
			? fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(AppHelper.toJSON)
			.then(data => {
				setCategories(data.categories)
				setCategoryName('')
				Swal.fire({
					title: 'Added Category!',
					text: 'You have successfully added a new category',
					icon: 'success',
                	background: '#121212',
                	backdrop: 'rgba(30, 30, 30, 0.9)'
				})
				.then((result) => {
					result.dismiss === Swal.DismissReason.timer
                    ? setShow(false)
                    : setShow(false)
				})
			})
			: 
	            Swal.fire({
	            	title: 'Category Exists!',
	            	text: 'You have already added this category',
	            	icon: 'error',
                	background: '#121212',
                	backdrop: 'rgba(30, 30, 30, 0.9)'
	            })
                .then((result) => {
                    result.dismiss === Swal.DismissReason.timer
                    ? null
                    : null
            })

		})
	}

	return (
		<React.Fragment>
			<a className="text-right text-purple" role="button" onClick={showModal}>Add Category</a>
	        <Modal show={show} onHide={closeModal} centered size="lg">
	            <Modal.Header closeButton className="modal-component">
	                <Modal.Title className="text-white">Add Category</Modal.Title>
	            </Modal.Header>

	            <Modal.Body className="modal-component">
	           		<Form onSubmit={(e) => addCategory(e)}>
						<Form.Row>
							<Col>
								<Form.Label className="text-muted">Category Name</Form.Label>
								<Form.Control
									className="input" 
									type="text" 
									placeholder="Category Name" 
									value={categoryName} 
									onChange={e => setCategoryName(e.target.value)} 
									required
								/>
							</Col>

							<Col>
								<Form.Label className="text-muted">Transaction Type</Form.Label>
					            <Form.Control 
					            	className="input" 
					            	as="select" 
					            	value={categoryType} 
					            	onChange={e => setCategoryType(e.target.value)} 
					            	required 
				            	>
					                <option>Expense</option>
					                <option>Income</option>
					            </Form.Control>
				            </Col>
							{ isActive === true
							    ? <Button type="submit" variant="success" block className="btn-purple mt-3"><span className="d-flex align-self-center justify-content-center">+</span></Button>
							    : <Button type="submit" variant="outline-success" block className="btn-purple-outline mt-3" disabled><span className="d-flex align-self-center justify-content-center">+</span></Button>}

						</Form.Row>
					</Form>
	            </Modal.Body>
	        </Modal>
		</React.Fragment>
	)
}