import React, { useState, useEffect } from 'react'
import { Container, Alert, Row, Col } from 'react-bootstrap'
import BarChart from '../../components/BarChart';
import LineChart from '../../components/LineChart';
import IncomePieChart from '../../components/IncomePieChart';
import ExpensePieChart from '../../components/ExpensePieChart';
import Head from 'next/head';
import NaviBar from '../../components/NaviBar';
import View from '../../components/View';
import * as FiIcons from "react-icons/fi";

export default function index() {

    const [ records, setRecords ] = useState([])

    // fetch user records hook
    useEffect(() => {
        fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {
            setRecords(data.transactions)
        })
    },[])

    return (
        <React.Fragment>
            <View title={ 'Analytics' }>
                <Row>
                    <Col lg={2} className="p-0">
                        <NaviBar />
                    </Col>

                    <Col lg={10} className="content-wrapper">
                        <Container className="my-5">
                            <h1 className="text-white"><FiIcons.FiBarChart /> Analytics</h1>
                            <Row>
                                <Col md={7}>
                                    <LineChart records={records} />
                                </Col>

                                <Col md={5}>
                                    <BarChart records={records} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6}>
                                    <IncomePieChart records={records} />
                                </Col>

                                <Col md={6}>
                                    <ExpensePieChart records={records} />
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                </Row>
            </View>
        </React.Fragment>
    )
}