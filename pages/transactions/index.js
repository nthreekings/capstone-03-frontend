import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import Link from 'next/link';
import { Container, Form, Row, Col, Card, Button } from 'react-bootstrap';
import AddRecord from '../../components/AddRecord';
import RecordList from '../../components/RecordList';
import NaviBar from '../../components/NaviBar';
import View from '../../components/View';
import * as FiIcons from "react-icons/fi";

export default function index() {
    const { user } = useContext(UserContext);
    const [categories, setCategories] = useState([]);
    const [records, setRecords] = useState([]);
    const [filteredRecords, setFilteredRecords] = useState([]);
    const [term, setTerm] = useState('');
    const [filter, setFilter] = useState('');
    const [recordOutput, setRecordOutput] = useState([]);
    const [currentBalance, setCurrentBalance] = useState('');

    // fetch user records and categories hook
    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {

            setRecords(data.transactions)
            setCategories(data.categories)

        })
    },[])

    useEffect(() => {

        setRecordOutput(records.map(record => {

                return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

        }).reverse())

    }, [records, categories])

    // filter 
    useEffect(() => {

        const updatedRecords = records.map(record => {

            if(record.categoryType === filter) {

                return record 

            } else if(filter === "All" || filter === '') {

                return record 

            } else {

                return null

            }

        })

        setFilteredRecords(updatedRecords)

        setRecordOutput(updatedRecords.map(record => {

            if(record !== null){

                if(record.categoryType === filter) {

                    return <RecordList key={record._id}records={record} categories={categories} setRecords={setRecords} />  

                } else if(filter === "All" || filter === '') {

                    return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

                }

            } else {

                return null

            }

        }).reverse())

    },[filter])

    // search
    useEffect(() => {

        if (term !== '') {

            let updatedRecords = filteredRecords.map((record) => {

                //console.log(record.description.toLowerCase().includes(term.toLowerCase()))

                if (record !== null){

                    if (record.description.toLowerCase().includes(term.toLowerCase())){
                    
                        return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

                    } 

                }

                else {

                    return null

                }
            })

            setRecordOutput(updatedRecords.reverse());


        } else {

            const updatedRecords = records.map(record => {

                return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

            })

            setRecordOutput(updatedRecords.reverse());

        }

    }, [term])

    return (
        <React.Fragment>
            <View title={ 'Transactions' }>
                <Row>
                    <Col lg={2} className="p-0">
                        <NaviBar />
                    </Col>

                    <Col lg={10} className="content-wrapper">
                        <Container className="my-5">
                            <h1 className="text-white"><span><FiIcons.FiRepeat /></span> Transactions</h1>
                            <Row>
                                <Col xs md="12">
                                    <AddRecord setRecords={setRecords} />
                                </Col>
                            </Row>
                            <Row>
                                <Col xs md="12">
                                    <Card className="card-component card-transactions my-2">
                                        <Card.Body>
                                            <Card.Title className="t-title ">
                                                <h2 className="text-white">Transaction Records</h2>
                                                <Form.Row>
                                                    <Col md="6">
                                                        <Form.Group controlId="search">
                                                            <Form.Control 
                                                                className="input"
                                                                type="text" 
                                                                placeholder="Search..."
                                                                onChange={(e) => setTerm(e.target.value)}
                                                                required
                                                            />
                                                        </Form.Group>
                                                    </Col>
                                                    <Col md="6">
                                                        <Form.Control
                                                            className="input"
                                                            as="select"
                                                            value={filter}
                                                            onChange={(e) => setFilter(e.target.value)}
                                                        
                                                            required
                                                        >
                                                            <option value="All">All</option>
                                                            <option value="Expense">Expense</option>
                                                            <option value="Income">Income</option>
                                                        </Form.Control>
                                                    </Col>
                                                </Form.Row>
                                                
                                            </Card.Title>
                                            {(records.length === 0)
                                            ?   <>   
                                                    <h5 className="text-muted text-center">No transactions yet</h5>
                                                </>
                                            :   <div className="transaction-container">
                                                    {recordOutput}
                                                </div>  
                                            }
                                        </Card.Body>
                                    </Card>
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                
                </Row>
            </View>

        </React.Fragment>
    )
}