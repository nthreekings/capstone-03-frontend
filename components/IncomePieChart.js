import React, { useState, useEffect } from 'react';
import { Form, Row, Col, Card } from 'react-bootstrap';
import { Pie } from 'react-chartjs-2';
import moment from 'moment';

export default function IncomePieChart({records}) {

    const [incomeRecords, setIncomeRecords] = useState([]);
    const [incomeCategories, setIncomeCategories] = useState([]);
    const [incomeAmount, setIncomeAmount] = useState([]);
    const [transactions, setTransactions] = useState([]);
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');

    // get all income transactions
    useEffect(() => {

        let incomeRecords = []

        records.filter((record) => {

            if(record.categoryType === "Income") {

                if(incomeRecords.indexOf(record.categoryName) === -1) {

                    incomeRecords.push(record)

                }

            }

        })

        setIncomeRecords(incomeRecords)

    },[records])

    // pre-load income records
    useEffect(() => {

        setTransactions(incomeRecords)

    },[incomeRecords])

    // get filtered transaction
    useEffect(() => {

        let filteredTransaction = []

        incomeRecords.forEach((record) => {

            if(startDate !== '' || endDate !== '') {

                if(moment(record.dateAdded).format('yyyy-MM-DD') >= moment(startDate).format('yyyy-MM-DD') && moment(record.dateAdded).format('yyyy-MM-DD') <= moment(endDate).format('yyyy-MM-DD')) {

                    filteredTransaction.push(record)

                }

            }

        })

        setTransactions(filteredTransaction)

    },[startDate, endDate])

    // get amount and labels
    useEffect(() => {

        let incomeCategories = []

        transactions.filter((transaction) => {

                if (incomeCategories.indexOf(transaction.categoryName) === -1) {

                    incomeCategories.push(transaction.categoryName)

                }   
            }

        )

        setIncomeCategories(incomeCategories)

        setIncomeAmount(incomeCategories.map((category) => {

            let amount = 0;

            transactions.forEach((transaction) => {

                if(transaction.categoryName === category)

                    amount += transaction.amount

            })

            return amount;

        }))

    },[transactions])

    const options = {
      maintainAspectRatio: false,
      responsive: false,
      legend: {
        position: 'left',
        labels: {
          boxWidth: 10
        }
      }
    }

    const colors = ["#6953F7","#CD4FF7","#F6AA65","#089450","#FB3BA1","#AA9EFA","#DE8AF9","#302D43","#C5D6D8","#CB958E","#99F7AB","#7E78D2","#8E9DCC","#7D84B2","#9EEFE5","#4F7CAC","#ABDF75","#C0E0DE","#F3B3A6","#CEFDFF","#B98B82","#D6F9DD","#E4959E","#C5D6D8","#CB958E","#99F7AB","#7E78D2"]

    return (
        <React.Fragment>
            <Card className="card-component mt-2 mb-5">
            
            <Card.Body>
                <Card.Title>
                    <h3 className="text-white">Income</h3>
                    <Form.Group>
                        <Row>
                            <Col xs md="6" className="my-1 pr-2">
                                {/*<Form.Label className="text-muted"> From </Form.Label>*/}
                                <Form.Control
                                    className="input"
                                    type="date"
                                    value={moment(startDate).format('yyyy-MM-DD')}
                                    onChange={e => setStartDate(e.target.value)}
                                />
                            </Col>

                            <Col xs md="6" className="my-1 pr-2">
                                {/*<Form.Label className="text-muted"> To </Form.Label>*/}
                                <Form.Control
                                    className="input"
                                    type="date"
                                    value={moment(endDate).format('yyyy-MM-DD')}
                                    onChange={e => setEndDate(e.target.value)}
                                />
                            </Col>
                        </Row>  
                    </Form.Group>
                </Card.Title>
                <div className="d-flex justify-content-center">
                {(incomeAmount.length === 0)
                    ? <h5 className="text-muted text-center mt-2">No transactions yet</h5>
                    : <Pie
                        options={options}

                        data = {{
                            labels: incomeCategories,
                            datasets: [

                                { 
                                    data: incomeAmount, 
                                    backgroundColor: colors,
                                    hoverBackgroundColor: colors,
                                    borderWidth: 0
                                }
                            
                            ]
                        }}

                        redraw={false}
                    />
                }
                </div>
            </Card.Body>
            </Card>
        </React.Fragment>
    )
}