import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import { Form } from 'react-bootstrap';
import AddRecord from '../../components/AddRecord';
import RecordList from '../../components/RecordList';
import Head from 'next/head';

export default function index() {
    const { user } = useContext(UserContext);
    const [name, setName] = useState('');
    const [categories, setCategories] = useState([]);
    const [records, setRecords] = useState([]);
    const [filteredRecords, setFilteredRecords] = useState([]);
    const [incomeSum, setIncomeSum] = useState([]);
    const [expenseSum, setExpenseSum] = useState([]);
    const [term, setTerm] = useState('');
    const [filter, setFilter] = useState('');
    const [recordOutput, setRecordOutput] = useState([]);
    const [currentBalance, setCurrentBalance] = useState('');

    
    // fetch user records and categories hook
    useEffect(() => {
        fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/details`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setName(data.firstName)
            setRecords(data.transactions)
            setCategories(data.categories)

            setRecordOutput(data.transactions.map(record => {

                return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

            }))

            // sum of all income
            const incomeCalc = data.transactions
                .filter(record => record.categoryType === 'Income')
                .map(record => record.amount)
                .reduce((total, amount) => total + amount, 0)
            setIncomeSum(incomeCalc)

            // sum of all expenses
            const expenseCalc = data.transactions
                .filter(record => record.categoryType === 'Expense')
                .map(record => record.amount)
                .reduce((total, amount) => total + amount, 0)
            setExpenseSum(expenseCalc)


        })
    },[])

    useEffect(() => {

        // new array for balance per transaction
        let balanceArr = records.map(record => {
            
            if(record !== null){

                return record.amount

            }

        }),sum
        balanceArr = balanceArr.map(recordAmount => sum = (sum || 0) + recordAmount)

        // get current total balance
        setCurrentBalance(balanceArr.slice(-1))

       /* // set colors for income/expense
        const incomeStyle = {color: "#588E29"}
        const totalIncomeStyle = {color: "#588E29"}
        const expenseStyle = {color: "#C8040E"}
        const totalExpenseStyle = {color: "#C8040E"}
        const amountStyle = (currentBalance >= 0) ? incomeStyle : expenseStyle*/


    },[records])

    // filter 
    useEffect(() => {

        console.log(records)

        const updatedRecords = records.map(record => {

            if(record.categoryType === filter) {

                return record 

            } else if(filter === "All" || filter === '') {

                return record 

            } else {

                return null

            }

        })

        setFilteredRecords(updatedRecords)

        setRecordOutput(updatedRecords.map(record => {

            if(record !== null){

                if(record.categoryType === filter) {

                    return <RecordList key={record._id}records={record} categories={categories} setRecords={setRecords} />  

                } else if(filter === "All" || filter === '') {

                    return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

                }

            } else {

                return null

            }

        }))

    },[filter])

    // filter 
    /*useEffect(() => {

        setRecordOutput(updatedRecords.map(record => {

            if(filter === "Expense" && record.categoryType === filter) {

                return <RecordList key={record._id}records={record} categories={categories} setRecords={setRecords} /> 

            } else if (filter === "Income" && record.categoryType === filter) {

                return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

            } else if(filter === "All" || filter === '') {

                return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

            }

        }))

    },[filter])*/

    // search
    useEffect(() => {

        if (term !== '') {

            console.log(filteredRecords)

            let updatedRecords = filteredRecords.map((record) => {

                //console.log(record.description.toLowerCase().includes(term.toLowerCase()))

                if (record !== null){

                    if (record.description.toLowerCase().includes(term.toLowerCase())){
                    
                        return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

                    } 

                }

                else {

                    return null

                }
            })

            setRecordOutput(updatedRecords);


        } else {

            const updatedRecords = records.map(record => {

                return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

            })

            setRecordOutput(updatedRecords);

        }

    }, [term])


    return (
        <React.Fragment>
            <Head>
                <title>Records</title>
            </Head>

            <h1>Hello, {name}!</h1>
            <h4>Your Current Balance is: PHP <span>{currentBalance}</span></h4>
            <h5>Total Income: PHP <span>{incomeSum}</span></h5>
            <h5>Total Expenses: PHP <span>{Math.abs(expenseSum)}</span></h5>

            <AddRecord categories={categories} setRecords={setRecords} />
            {(records.length === 0)
                ?   <p>No transaction records yet</p>
                :   <>
                    <Form.Group controlId="search">
                        <Form.Control 
                            type="text" 
                            placeholder="Search Transaction"
                            onChange={(e) => setTerm(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Control
                        as="select"
                        value={filter}
                        onChange={(e) => setFilter(e.target.value)}
                    
                        required
                    >
                        <option value="All">All</option>
                        <option value="Expense">Expense</option>
                        <option value="Income">Income</option>
                    </Form.Control>
                    {recordOutput}
                     
                    </>
            }   

        </React.Fragment>
    )
}