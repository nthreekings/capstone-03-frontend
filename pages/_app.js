import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Router from 'next/router';
import { useRouter } from 'next/router';
import Loading from '../components/Loading';
import { UserProvider } from '../UserContext';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {

	// hook for the loading screen
    const [ isLoading, setIsLoading ] = useState(false);

	// State hook for the user details
	const [user, setUser] = useState({id: null})

	// Clear local storage and set user state to null upon logout
	const unsetUser = () => {

		localStorage.clear()

		setUser({id: null})
	}

	Router.onRouteChangeStart = () => {
	    console.log('onRouteChangeStart Triggered');
	   	<Loading />;
	   	setIsLoading(true)
	};

	Router.onRouteChangeComplete = () => {
	    console.log('onRouteChangeComplete Triggered');
	    <Loading />;
	    setIsLoading(false)
	};

	Router.onRouteChangeError = () => {
	    console.log('onRouteChangeError Triggered');	    
	    <Loading />;
	    setIsLoading(true)

	};

	// Get user details from API upon user ID state change
	useEffect(() => {
		fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/details`, {
		  headers: {
		    'Authorization': `Bearer ${localStorage.getItem('token')}`
		  }
		})
		.then(res => res.json())
		.then(data => {

		  (data._id) 
		  ? setUser({id:data._id}) 
		  : setUser({id: null})
		  
		})
	}, [user.id])


	return (
		<React.Fragment>
			<UserProvider value={{user, setUser, unsetUser}}>
				

				<Col className="page-content">
					{isLoading 
						? <Loading />
						: <Component {...pageProps} />
					}
				</Col>
				
			</UserProvider>
		</React.Fragment>
	)
}

export default MyApp