import React, { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'
import { Button, Form, Modal, Row, Col } from 'react-bootstrap'
import Swal from 'sweetalert2'


export default function UpdateRecord({records, categories, setRecords}) {
    const {user} = useContext(UserContext);
    const [categoryId, setCategoryId] = useState('');
    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
    const [amount, setAmount] = useState('');
    const [description, setDescription] = useState('');
    const [transactionCategories, setTransactionCategories] = useState([]);;
    const [isActive, setIsActive] = useState(false);
    const [ show, setShow ] = useState(false);
    const showModal = () => setShow(true);

    // reset update form upon modal close
    const closeModal = () => {
        setShow(false)
        setDescription('')
        setAmount('')
    }

    // pre-load category name and type once category prop gets passed hook
    useEffect(()=> {
        (categories.length === 0)
        ? setCategoryName('')
        : setCategoryName(records.categoryName)
    },[categories])

    // verify record amount hook
    useEffect(()=> {
        ((amount.length > 0 && isNaN(amount) === false) && (categoryType.length > 0 && categoryName.length > 0))
        ? setIsActive(true)
        : setIsActive(false)
    }, [amount, categoryName, categoryType])

     // auto-set category ID after choosing category name hook
    useEffect(()=> {
        const autoId = categories.find(autoId => autoId.categoryName === categoryName)
        return (
            (autoId === undefined)
            ? setCategoryId('')
            : setCategoryId(autoId._id)
        )
    },[categoryName])

    // set category name options based on category type
    useEffect(() => {

        let categoryOptions =  categories.map(category => {

            if (category.categoryType === categoryType) {

                return(
                    <option key={category._id}>{category.categoryName}</option>
                )

            } else {

                return null

            }
            
        })

        setTransactionCategories(categoryOptions);

    }, [categoryType])

    const updateRecord = (e) => {
        
        e.preventDefault()

        fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/${user.id}/tr/${records._id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                categoryId: categoryId,
                categoryName: categoryName,
                categoryType: categoryType,
                amount: amount,
                description: description
            })
        })
        .then((res => res.json()))
        .then((data) => {
            data
            ?   fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/details`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
                })
                .then(res => res.json())
                .then(data => {
                    setRecords(data.transactions)
                    setAmount('')
                    setDescription('')
                    Swal.fire({
                        title: 'Updated record!',
                        text: 'Record is updated successfully',
                        icon: 'success',
                        background: '#121212',
                        backdrop: 'rgba(30, 30, 30, 0.9)'
                    })
                    .then((result) => {
                        result.dismiss === Swal.DismissReason.timer
                        ? setShow(false)
                        : setShow(false)
                    })
                })
            :   Swal.fire({
                    title: 'Update error!',
                    text: 'Something went wrong',
                    icon: 'error',
                    background: '#121212',
                    backdrop: 'rgba(30, 30, 30, 0.9)'
                })
                .then((result) => {
                    result.dismiss === Swal.DismissReason.timer
                    ? null
                    : null
                })
        })
    }

    return(
        <>
        <a className="text-right text-purple" role="button" onClick={showModal}><small>Edit</small></a>
        <Modal show={show} onHide={closeModal} centered size="lg">
            <Modal.Header closeButton className="modal-component">
                <Modal.Title className="text-white">Update Record</Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-component">
            <Form onSubmit={(e) => updateRecord(e)}>
            <Form.Row>
                <Col>
                    <Form.Row>
                        <Col>
                            <Form.Label className="text-muted" column>Name:</Form.Label>
                            <Col>
                                <Form.Control 
                                    className="input"
                                    type="text" 
                                    placeholder={records.description} 
                                    value={description} 
                                    onChange={e => setDescription(e.target.value)} 
                                    required 
                                />
                            </Col>
                        </Col>

                        <Col>
                            <Form.Label className="text-muted" column>Amount:</Form.Label>
                            <Col>
                                <Form.Control 
                                    className="input"
                                    type="text" 
                                    placeholder={Math.abs(records.amount)} 
                                    value={amount} 
                                    onChange={e => setAmount(e.target.value)} 
                                    required 
                                />
                            </Col>
                        </Col>
                        
                    </Form.Row>
                    
                    <Form.Row>
                        
                        <Col>
                            {/*<Form.Label className="text-muted" column>Type:</Form.Label>*/}
                            <Col>
                                {/*<Form.Control
                                    className="input"
                                    as="select"
                                    value={categoryType}
                                    onChange={(e) => setCategoryType(e.target.value)}
                                
                                    required
                                >
                                    <option>{transactionCategories.categoryType}</option>
                                    <option value="Expense">Expense</option>
                                    <option value="Income">Income</option>
                                </Form.Control>*/}
                                <Form.Label className="text-muted">Transaction Type</Form.Label>
                                <Form.Control
                                    className="input"
                                    as="select"
                                    value={categoryType}
                                    onChange={(e) => setCategoryType(e.target.value)}
                                
                                    required
                                >
                                    <option>Select category..</option>
                                    <option value="Expense">Expense</option>
                                    <option value="Income">Income</option>
                                </Form.Control>
                            </Col>
                        </Col>

                        <Col>
                            {/*<Form.Label className="text-muted" column>Category:</Form.Label>
                            <Col>
                                <Form.Control 
                                    className="input"
                                    as="select"
                                    value={categoryName}
                                    onChange={(e) => setCategoryName(e.target.value)}
                                    required 
                                >   
                                    
                                    <option>{records.categoryName}</option>
                                    {transactionCategories}
                                    
                                </Form.Control>
                            </Col>*/}
                            <Form.Label className="text-muted">Transaction Category</Form.Label>
                            <Form.Control 
                                className="input"
                                as="select"
                                value={categoryName}
                                onChange={(e) => setCategoryName(e.target.value)}
                                required 
                            >   
                                
                                <option>Select category..</option>
                                {transactionCategories}
                                
                            </Form.Control>
                        </Col>
                      
                    </Form.Row>
                    
                    <Form.Row>
                        <Col>
                            { isActive === true
                            ? <Button type="submit" block className="btn-purple mt-3">Update Transaction</Button>
                            : <Button type="submit" block className="btn-purple-outline mt-3" disabled>Update Transaction</Button>
                            }
                        </Col>
                    </Form.Row>
            </Col>
                
            </Form.Row>
            </Form>
            </Modal.Body>
        </Modal>
        </>
    )
}