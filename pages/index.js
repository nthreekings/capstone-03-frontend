import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import Link from 'next/link';
import View from '../components/View';
import AppHelper from '../app-helper';
import { Container, Form, Button, Row, Col, Image } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Home() {

    return(
        <View title={ 'FruPal' }>
            <Container className="login-page d-flex justify-content-center align-items-center">
                
                <Row>
                    <Col>
                        <Row className="justify-content-center">            
                            <Image className="logo-icon my-2" src="/logo-icon.svg" />
                        </Row>

                        <Row>
                            <div className="login-form">
                                <h3 className="text-white text-center">Login</h3>
                                <LoginForm />
                            </div>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </View>
    )

}

const LoginForm = () => {

    
    const { user, setUser} = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        const options = {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })

        }

        fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data => {

            if (typeof data.accessToken !== 'undefined'){

                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)

            } else {

                if (data.error === 'does-not-exist'){

                    Swal.fire({
                        title:'Authentication Failed', 
                        text: 'User does not exist',
                        icon: 'error',
                        background: '#121212',
                        backdrop: 'rgba(30, 30, 30, 0.9)'
                    })

                } else if (data.error === 'incorrect-password') {

                    Swal.fire({
                        title: 'Authentication Failed', 
                        text: 'Password is incorrect', 
                        icon: 'error',
                        background: '#121212',
                        backdrop: 'rgba(30, 30, 30, 0.9)'
                    })

                } else if (data.error === 'login-type-error') {

                    Swal.fire({
                        title: 'Login Type Error', 
                        text: 'You may have registered through a different login procedure, try the alternative login procedures.', 
                        icon: 'error',
                        background: '#121212',
                        backdrop: 'rgba(30, 30, 30, 0.9)'
                    })

                }

            }

        })

    }

    const authenticateGoogleToken = (response) => {

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ 
                tokenId: response.tokenId 
                })
        }   

        /*const payload = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : response.tokenId
            }
        }*/

        fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/verify-google-id-token`, payload)
        .then(AppHelper.toJSON)
        .then(data => {

            if (typeof data.accessToken !== 'undefined') {

                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)

            } else {

                if (data.error === 'google-auth-error') {
                    Swal.fire({
                        title: 'Google Auth Error',
                        text: 'Google authentication failed',
                        icon: 'error',
                        background: '#121212',
                        backdrop: 'rgba(30, 30, 30, 0.9)'
                    })
                } else if (data.error === 'login-type-error') {
                    Swal.fire({
                        title: 'Login Type Error',
                        text: 'You may have registered through a different login procedure',
                        icon: 'error',
                        background: '#121212',
                        backdrop: 'rgba(30, 30, 30, 0.9)'
                    })
                }

            }

        })
    
    };


    /*const failed = (response) => {

        console.log(response)

    };*/

    const retrieveUserDetails = (accessToken) => {

        const options = {

            headers: { Authorization: `Bearer ${ accessToken }` }

        }

        fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data => {

            setUser({ id: data._id, isAdmin: data.isAdmin })
            Router.push('/dashboard')

        })

    }

    return (
        <React.Fragment>

            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label className="text-muted">Email address</Form.Label>
                    <Form.Control 
                        className="input" 
                        type="email" 
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label className="text-muted">Password</Form.Label>
                    <Form.Control
                        className="input" 
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                <>
                <Button type="submit" id="submitBtn" className="btn main-btn w-100 mb-2 text-center d-flex justify-content-center align-center">
                    <span className="d-flex align-self-center">Login</span>
                </Button>
                <p className="text-center text-muted"> ⎯⎯⎯⎯ or ⎯⎯⎯⎯ </p>
                <GoogleLogin 
                    // clientId = OAuthClient id from Cloud Google developer platform
                    clientId="727338770716-7psh1e30fuhgifgs9te9d9vs1tjidjiq.apps.googleusercontent.com"
                    buttonText="Login with Google"
                    // onSuccess = it runs a function w/c returns a Google user object w/c provides access to all of the Google user method and details
                    onSuccess={ authenticateGoogleToken }
                    onFailure={ authenticateGoogleToken } // alternative { failed }
                    // cookiePolicy = determines cookie policy for the origin of the Google login requests
                    cookiePolicy={ 'single_host_origin' }
                    className="btn w-100 text-center d-flex justify-content-center"
                />
                </>
                    
                <p className="text-muted text-center mt-3">
                    Don't have an account?&nbsp;
                    <span>
                        <Link href="/register">
                            <a className="text-white text-link">Create Account</a>
                        </Link>
                    </span>
                </p>
            </Form>
        </React.Fragment>
    )
}