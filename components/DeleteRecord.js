import { useContext } from 'react';
import UserContext from '../UserContext';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteRecord({recordId, setRecords}) {

    const {user} = useContext(UserContext)
    
    const deleteRecord = (recordId) => {
        Swal.fire({
            text: 'Are you sure you want to delete this record?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            confirmButtonColor: '#FB3BA1',
            cancelButtonText: 'Cancel',
            background: '#121212',
            backdrop: 'rgba(30, 30, 30, 0.9)'
        })
        .then((result) => {
            result.value
                ?   fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/${user.id}/tr/${recordId}`, {
                    method: 'DELETE',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    }
                    })
                    .then((res => res.json()))
                    .then((data) => {
                        data
                        ?   fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/details`, {
                            headers: {
                                Authorization: `Bearer ${localStorage.getItem('token')}`
                            }
                            })
                            .then(res => res.json())
                            .then(data => {
                                setRecords(data.transactions)
                                Swal.fire({
                                    title: 'Deleted record!',
                                    icon: 'success',
                                    background: '#121212',
                                    backdrop: 'rgba(30, 30, 30, 0.9)'
                                })
                                .then((result) => {
                                    result.dismiss === Swal.DismissReason.timer
                                    ? null
                                    : null
                                })
                            })
                        :   Swal.fire({
                                title: 'Delete error!',
                                icon: 'error',
                                background: '#121212',
                                backdrop: 'rgba(30, 30, 30, 0.9)'
                            })
                            .then((result) => {
                                result.dismiss === Swal.DismissReason.timer
                                ? null
                                : null
                            })
                    })
            : null
        })
    }

    return(
        <a className="text-right text-delete" role="button" onClick={() => deleteRecord(recordId)} ><small>Delete</small></a>
    )
}