import React, { useState, useEffect } from 'react';
import { Form, Row, Col, ListGroup, Dropdown } from 'react-bootstrap';
import DeleteRecord from './DeleteRecord';
import UpdateRecord from './UpdateRecord';
import * as FiIcons from "react-icons/fi";

export default function RecordList({records, categories, setRecords}) {

    const recordDate = new Date(records.dateAdded).toLocaleString('en-US',{
                    month:'short',
                    day:'numeric',
                    year:'numeric',
                    //weekday:'long',
                    hour:'2-digit',
                    minute:'2-digit'
                })

    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
      <a
        href=""
        ref={ref}
        onClick={e => {
          e.preventDefault();
          onClick(e);
        }}
      >
        {/* custom icon */}
        {children}
        <span><FiIcons.FiMoreHorizontal className="threedots"/></span>
      </a>
    ));

    return (
        <React.Fragment>
            <div>
            <ListGroup>
                <ListGroup.Item className="transaction-record list-record my-2">
                    <Row>
                    <Col>
                    <p className="text-muted mb-0 text-left"><small>{recordDate}</small></p>
                    </Col>
                        <Col  className="d-flex justify-content-end">
                            <Dropdown >
                                <Dropdown.Toggle as={CustomToggle}>
                                </Dropdown.Toggle>
                                <Dropdown.Menu size="sm" title=""> 
                                  <Dropdown.Item><UpdateRecord records={records} categories={categories} setRecords={setRecords} /></Dropdown.Item>
                                  <Dropdown.Item><DeleteRecord recordId={records._id} setRecords={setRecords} /></Dropdown.Item>
                                </Dropdown.Menu>
                             </Dropdown>
                        </Col>
                    </Row>

                    <Row>
                        <Col>

                            <h5 className="text-white mb-0"><strong>{records.description}</strong></h5>
                            <p className="text-white mb-0"><small>{records.categoryName}</small></p>
                                
                        </Col>

                        <Col className="d-flex align-items-center justify-content-end"> 
                                    
                            {(records.categoryType === "Income")
                            ?   <h5 className="amount-income mb-0 text-right"><strong>+ ₱ {Math.abs(records.amount)}</strong></h5>
                            :   <h5 className="amount-expense mb-0 text-right"><strong>- ₱ {Math.abs(records.amount)}</strong></h5>
                            }
                                        
                        </Col>
                    </Row>
                </ListGroup.Item>
            </ListGroup>
            
            </div>
        </React.Fragment>
    )
    
}