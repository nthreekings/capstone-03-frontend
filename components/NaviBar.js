import React, { useState, useContext, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Image } from 'react-bootstrap';
import UserContext from '../UserContext';
import * as FiIcons from "react-icons/fi";

export default function NaviBar() {

	const { user } = useContext(UserContext);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [currentBalance, setCurrentBalance] = useState('');

    const [router, setRouter] = useState(useRouter());

    // fetch user records and categories hook
    useEffect(() => {
        fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/details`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {
            setFirstName(data.firstName)
            setLastName(data.lastName)

            const total = data.transactions.reduce((a, b) => {
                return a + (b.categoryType === 'Expense' ? (-b.amount) : b.amount);
            }, 0);
            setCurrentBalance(total);

        })
    },[])

	return (
		<div className="sidebar">
		<Navbar sticky="top" expand="lg">
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="flex-column w-100">
				
				<React.Fragment>
					<Link href="/dashboard">
						<Image className="logo d-flex align-self-center my-3" src="/logo-name.svg" />
					</Link>
					<Image className="avatar d-flex align-self-center mt-3" src="/avatar.svg" />
					<h6 className="text-white mt-2 w-100 text-center">{firstName} {lastName}</h6>
					<h5 className="text-purple w-100 text-center">PHP {currentBalance}</h5>
					<Link href="/dashboard">
					  	<a className={router.pathname === "/dashboard" ? "nav-link btn-purple text-left mt-5 w-100" : "nav-link text-white text-left mt-5 w-100"} role="button"><span className="mr-2"><FiIcons.FiHome className="icon-link"/></span> Home</a>
					  </Link>
					<Link href="/transactions">
					  	<a className={router.pathname === "/transactions" ? "nav-link btn-purple text-left mt-3 w-100" : "nav-link text-left text-white mt-3 w-100"} role="button"><span className="mr-2"><FiIcons.FiRepeat className="icon-link"/></span> Transactions</a>
				  	</Link>
				  	<Link href="/analytics">
					  	<a className={router.pathname === "/analytics" ? "nav-link btn-purple text-left mt-3 w-100" : "nav-link text-left text-white mt-3 w-100"} role="button"><span className="mr-2"><FiIcons.FiBarChart className="icon-link"/></span> Analytics</a>
				  	</Link>
				  	<Link href="/settings">
					  	<a className={router.pathname === "/settings" ? "nav-link btn-purple text-left mt-3 w-100" : "nav-link text-left text-white mt-3 w-100"} role="button"><span className="mr-2"><FiIcons.FiSettings className="icon-link"/></span> Settings</a>
				  	</Link>
					<Link href="/logout">
				  		<a className="text-left nav-link text-white my-3 w-100" role="button" ><span className="mr-2"><FiIcons.FiLogOut className="icon-link"/></span> Logout</a>
				  	</Link>
			  	</React.Fragment>

		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
		</div>
	)
}