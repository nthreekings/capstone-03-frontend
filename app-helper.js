// The goal of this helper file is to shorten written code in various parts of the project
// If you want to create usable functions/block of code that you can use in your project

module.exports = {
	API_URL: process.env.NEXT_PUBLIC_API_URL,
	getAccessToken: () => localStorage.getItem('token'),
	toJSON: (res) => res.json()
}