import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import View from '../../components/View';
import Link from 'next/link';

export default function index() {

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	function registerUser(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		fetch(`https://arcane-thicket-72170.herokuapp.com/api/users/email-exists`, {
			method: 'POST',
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(AppHelper.toJSON)
		.then(data => {

			if (data === false) {

				fetch(`https://arcane-thicket-72170.herokuapp.com/api/users`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNumber,
						password: password1
					})
				})
				.then(AppHelper.toJSON)
				.then(data => {

					if (data === true) {
						Swal.fire({
							title: 'Congratulations!',
							text: 'You have registered successfully. You will be redirected to login',
							icon: 'success',
	                    	background: '#121212',
	                    	backdrop: 'rgba(30, 30, 30, 0.9)'
						})

						// Redirect to login
						setTimeout(function(){window.location.replace("./")}, 2000)
					} else {
						Swal.fire({
							title: 'Uh Oh!',
							text: 'Something went wrong',
							icon: 'error',
	                    	background: '#121212',
	                    	backdrop: 'rgba(30, 30, 30, 0.9)'
						})
					}
				})

			} else {

				Swal.fire({
					title: 'Duplicate Email Found!',
					text: 'This email is already registered',
					icon: 'error',
                	background: '#121212',
                	backdrop: 'rgba(30, 30, 30, 0.9)'
				})

			}

		})

		
	}

	return (

		<React.Fragment>
			<View title={ 'Register' }>
	        	<Container className="login-page my-5">
		            <Row className="justify-content-center">
		                <Col xs md="6" className="login-form">
		                    <h3 className="text-white text-center">Create an account</h3>
		                    <Form onSubmit={(e) => registerUser(e)}>
								<Form.Group controlId="firstName">
									<Form.Label className="text-muted">First Name</Form.Label>
									<Form.Control
										className="input" 
										type="text" 
										placeholder="Enter your first name"
										value={firstName}
										onChange={(e) => setFirstName(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="lastName">
									<Form.Label className="text-muted">Last Name</Form.Label>
									<Form.Control 
										className="input"
										type="text" 
										placeholder="Enter your last name"
										value={lastName}
										onChange={(e) => setLastName(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="mobileNumber">
									<Form.Label className="text-muted">Mobile Number</Form.Label>
									<Form.Control 
										className="input"
										type="text" 
										placeholder="Enter your mobile number"
										value={mobileNumber}
										onChange={(e) => setMobileNumber(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="userEmail">
									<Form.Label className="text-muted">Email Address</Form.Label>
									<Form.Control 
										className="input"
										type="email" 
										placeholder="Enter your email"
										value={email}
										onChange={(e) => setEmail(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="password1">
									<Form.Label className="text-muted">Password</Form.Label>
									<Form.Control
										className="input"
										type="password"
										placeholder="Enter Password"
										value={password1}
										onChange={(e) => setPassword1(e.target.value)}
										required
									/>
								</Form.Group>

								<Form.Group controlId="password2">
									<Form.Label className="text-muted">Verify Password</Form.Label>
									<Form.Control
										className="input"
										type="password"
										placeholder="Verify Password"
										value={password2}
										onChange={(e) => setPassword2(e.target.value)}
										required
									/>
								</Form.Group>
									
								<Button type="submit" id="submitBtn" className="btn main-btn w-100 mb-2 text-center d-flex justify-content-center align-center">
									<span className="d-flex align-self-center">Create Account</span>
								</Button>
								
								<p className="text-muted text-center mt-3">
								    Already have an account?&nbsp;
								    <span>
								        <Link href="/">
								            <a className="text-white text-link">Login</a>
								        </Link>
								    </span>
								</p>

							</Form>
		                </Col>
		            </Row>
	            </Container>
	        </View>
		</React.Fragment>

	)
}