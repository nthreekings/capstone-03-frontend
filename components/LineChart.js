import React, { useState, useEffect } from 'react';
import { Form, Row, Col, Card } from 'react-bootstrap';
import { Line } from 'react-chartjs-2';
import moment from 'moment';

export default function LineChart({records}) {

    const [transactions, setTransactions] = useState([]);
    const [amountRecords, setAmountRecords] = useState([]);
    const [amount, setAmount] = useState([]);
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');

    // get all amount
    useEffect(() => {

        let amountRecords = records.map((record) => {
            
            if(record.categoryType === 'Expense') {
                
                return -record.amount

            } else {

                return record.amount

            }

        })

        setAmountRecords(amountRecords)

    }, [records])

    // pre-load expense records
    useEffect(() => {

        setTransactions(amountRecords)

    },[amountRecords])

    // get filtered transaction
    useEffect(() => {

        let filteredTransaction = []

        records.forEach((record) => {

            if(startDate !== '' || endDate !== '') {

                 if(moment(record.dateAdded).format('yyyy-MM-DD') >= moment(startDate).format('yyyy-MM-DD') && moment(record.dateAdded).format('yyyy-MM-DD') <= moment(endDate).format('yyyy-MM-DD')) {

                    if(record.categoryType === 'Expense') {
                
                        filteredTransaction.push(-record.amount)

                    } else {

                        filteredTransaction.push(record.amount)

                    }
                    

                }

            }   

        })

        setTransactions(filteredTransaction)

    },[startDate, endDate])

    // map and sort records by date added. Sort just to make sure records are chronological
    const chartDates = records.map(record => new Date(record.dateAdded).toLocaleString('en-US',{
        month:'short',
        //day:'numeric',
        //hour: '2-digit'
    }))
    .sort((a,b) => b - a)

    useEffect(() => {

        let sum = transactions.map((transaction) => {
            return sum = (sum || 0) + transaction
        })

        setAmount(sum)


    },[transactions])

    return (
        <React.Fragment>
            <Card className="card-component chart-component my-4">
                <Card.Body>
                    <Card.Title>
                        <h3 className="text-white">Balance Trend</h3>
                        <Form.Group>
                            <Row>
                                <Col xs md="6" className="my-1 pr-2">
                                    {/*<Form.Label className="text-muted"> From </Form.Label>*/}
                                    <Form.Control
                                        className="input"
                                        type="date"
                                        value={moment(startDate).format('yyyy-MM-DD')}
                                        onChange={e => setStartDate(e.target.value)}
                                    />
                                </Col>

                                <Col xs md="6" className="my-1 pr-2">
                                    {/*<Form.Label className="text-muted"> To </Form.Label>*/}
                                    <Form.Control
                                        className="input"
                                        type="date"
                                        value={moment(endDate).format('yyyy-MM-DD')}
                                        onChange={e => setEndDate(e.target.value)}
                                    />
                                </Col>
                            </Row>  
                        </Form.Group>
                    </Card.Title>

                    {(amount.length === 0)
                    ? <h5 className="text-muted text-center mt-2">No transactions yet</h5>
                    : <Line
                            options={{ maintainAspectRatio: false }}
                            
                            data={{
                                datasets:[{
                                    label: 'Balance',
                                    fill: true,
                                    data: amount,
                                    backgroundColor: 'rgba(255,99,132,0.2)',
                                    pointBorderColor: '#FB3BA1',
                                    borderColor: '#FB3BA1'
                                }],
                                labels: amount
                            }}

                            redraw={false}
                        />
                    }
                    
                </Card.Body>
            </Card>
        </React.Fragment>
    )

}