import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext'
import { Card, Row, Col, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditPassword() {

	const {user} = useContext(UserContext);
	const [userID, setUserID] = useState('');
	const [current, setCurrent] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [password, setPassword] = useState('');

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
		    "method": "GET",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {
			setUserID(data._id)
		});
	}, []);

	console.log(password)

	function updatePassword(e){

		e.preventDefault();

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/password-check`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					userID: userID,
					password: current
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === false) {
					Swal.fire({
		                title: 'Empty!',
		                text: 'Please enter current password',
		                icon: 'error',
		                background: '#121212',
		                backdrop: 'rgba(30, 30, 30, 0.9)'
		            })
		            .then((result) => {
		                result.dismiss === Swal.DismissReason.timer
		                ? null
		                : null
		            })

				} else {

					if ((password1 === password2) && (password1 !== '' && password2 !== '')) {

							setPassword(password1)

					} else if ((password1 === '' && password2 === '')) {

						Swal.fire({
			                title: 'Empty Password!',
			                text: 'Password cannot be empty',
			                icon: 'error',
			                background: '#121212',
			                backdrop: 'rgba(30, 30, 30, 0.9)'
			            })
			            .then((result) => {
			                result.dismiss === Swal.DismissReason.timer
			                ? null
			                : null
			            })
					} else {

						Swal.fire({
			                title: 'Uh oh!',
			                text: 'Password does not match',
			                icon: 'error',
			                background: '#121212',
			                backdrop: 'rgba(30, 30, 30, 0.9)'
			            })
			            .then((result) => {
			                result.dismiss === Swal.DismissReason.timer
			                ? null
			                : null
			            })

					}

					fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${userID}`, {
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							'Authorization' : `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							userID : userID,
							password: password
						})
					})
					.then(res => res.json())
					.then(data => {

						if(data){
							Swal.fire({
				                title: 'Updated password!',
				                text: 'Password is updated successfully',
				                icon: 'success',
				                background: '#121212',
				                backdrop: 'rgba(30, 30, 30, 0.9)'
				            })
				            .then((result) => {
				                result.dismiss === Swal.DismissReason.timer
				                ? null
				                : null
				            }) 
						}
						else{
							Swal.fire({
				                title: 'Update error!',
				                text: 'Something went wrong',
				                icon: 'error',
				                background: '#121212',
				                backdrop: 'rgba(30, 30, 30, 0.9)'
				            })
				            .then((result) => {
				                result.dismiss === Swal.DismissReason.timer
				                ? null
				                : null
				            })
						}
					})
				}
			})
	}

	return(
		<React.Fragment>
			<Card className="card-component">
				<Card.Body>
					<Row>
						<Col md="4">
							<h4 className="my-3 text-white">Edit Password</h4>
						</Col>

						<Col md="8">
							<Form onSubmit={(e) => updatePassword(e)}>
								<Form.Row>
									<Col md="12">
										<Form.Label className="mt-3 text-muted">Current Password</Form.Label>
	                                    <Form.Control 
	                                        className="input"
	                                        type="password" 
	                                        placeholder="Enter Current Password"
	                                        value={current}
	                                        onChange={(e) => setCurrent(e.target.value)}
	                                        required
	                                    />
                                	</Col>
								</Form.Row>

								<Form.Row>
									<Col md="6">
										<Form.Label className="mt-3 text-muted">New Password</Form.Label>
	                                    <Form.Control 
	                                        className="input"
	                                        type="password" 
	                                        placeholder="Enter New Password"
	                                        value={password1}
	                                        onChange={(e) => setPassword1(e.target.value)}
	                                        required
	                                    />
		                                
									</Col>

									<Col md="6">
										<Form.Label className="mt-3 text-muted">Verify New Password</Form.Label>
	                                    <Form.Control 
	                                        className="input"
	                                        type="password"
	                                        placeholder="Verify New Password"
	                                        value={password2}
	                                        onChange={(e) => setPassword2(e.target.value)}
	                                        required
	                                    />
									</Col>
									<Button type="submit" block variant="success" className="btn-purple my-3"><span className="d-flex align-self-center justify-content-center">Update Password</span>
						  			</Button>
								</Form.Row>
							</Form>
						</Col>
					</Row>
				</Card.Body>
			</Card>
		</React.Fragment>
	)
}