import { Row, Col, ListGroup } from 'react-bootstrap';

export default function CategoryList({ categories, setCategories }) {

	// set colors for income/expense
    const incomeStyle = {color: "#588E29"}
    const expenseStyle = {color: "#C8040E"}

    return (
        <ListGroup>
            {categories.map(category => {

                const categoryStyle = (category.categoryType === 'Income') ? incomeStyle : expenseStyle

                return (
                    <ListGroup.Item key={category._id}>
                    <Row>
                        <Col>{category.categoryName}</Col>
                        <Col style={categoryStyle}>{category.categoryType}</Col>
                    </Row>
                    </ListGroup.Item>
                )
            })}
        </ListGroup>
    )

}