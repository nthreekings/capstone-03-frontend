import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import Link from 'next/link';
import { Container, Form, Row, Col, Card, Button } from 'react-bootstrap';
import EditProfile from '../../components/EditProfile';
import EditPassword from '../../components/EditPassword';
import NaviBar from '../../components/NaviBar';
import View from '../../components/View';
import * as FiIcons from "react-icons/fi";

export default function index() {

    return(
        <React.Fragment>
            <View title={ 'Transactions' }>
                <Row>
                    <Col lg={2} className="p-0 sidebar-content">
                        <NaviBar />
                    </Col>

                    <Col lg={10} className="content-wrapper">
                        <Container className="my-5">
                            <h1 className="text-white"><FiIcons.FiSettings /> Settings</h1>
                            <Row>
                                <Col>
                                    <EditProfile />
                                </Col>
                            </Row>

                            <Row>
                                <Col>
                                    <EditPassword />
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                
                </Row>
            </View>

        </React.Fragment>
    )

}