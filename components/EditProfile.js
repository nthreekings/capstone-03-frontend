import React, { useState, useEffect } from 'react';
import { Card, Row, Col, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditProfile() {

	const [userID, setUserID] = useState('');
	const [email, setEmail] = useState('');
	const [userName, setUsername] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
		    "method": "GET",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {
			setUserID(data._id)
			setEmail(data.email || '');
			setUsername(data.userName || '');
			setFirstName(data.firstName || '');
			setLastName(data.lastName || '');
			setMobileNo(data.mobileNo || '');

		});
	}, []);

	function updateUserInfo(e){
		e.preventDefault();
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/`, {
		    "method": "PUT",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`,
		      "Content-Type": 'application/json'
		    },
		    body: JSON.stringify({
		    	userID: userID,
				firstName: firstName,
				email: email,
				lastName: lastName,
				mobileNo: mobileNo
		    })
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
	                title: 'Updated profile!',
	                text: 'Profile is updated successfully',
	                icon: 'success',
	                background: '#121212',
	                backdrop: 'rgba(30, 30, 30, 0.9)'
	            })
	            .then((result) => {
	                result.dismiss === Swal.DismissReason.timer
	                ? null
	                : null
	            }) 
			}
			else{
				Swal.fire({
	                title: 'Update error!',
	                text: 'Something went wrong',
	                icon: 'error',
	                background: '#121212',
	                backdrop: 'rgba(30, 30, 30, 0.9)'
	            })
	            .then((result) => {
	                result.dismiss === Swal.DismissReason.timer
	                ? null
	                : null
	            })
			}

		});
	}

	return(
		<React.Fragment>
			<Card className="card-component my-4">
				<Card.Body>
					<Row>
						<Col md="4">
							<h4 className="my-3 text-white">Edit Profile</h4>
						</Col>

						<Col md="8">
							<Form onSubmit={(e) => updateUserInfo(e)}>
								<Form.Row>
									<Col md="6">
										<Form.Label className="mt-3 text-muted">First Name</Form.Label>
	                                    <Form.Control 
	                                        className="input"
	                                        type="text" 
	                                        placeholder={firstName}
	                                        value={firstName}
											onChange={(e) => setFirstName(e.target.value)}
	                                        required
	                                    />
		                                
									</Col>

									<Col md="6">
										<Form.Label className="mt-3 text-muted">Last Name</Form.Label>
	                                    <Form.Control 
	                                        className="input"
	                                        type="text" 
	                                        placeholder={lastName}
	                                        value={lastName}
											onChange={(e) => setLastName(e.target.value)}
	                                        required
	                                    />
									</Col>
								</Form.Row>
								<Form.Row>
									<Col  md="6">
										<Form.Label className="mt-3 text-muted">Mobile Number</Form.Label>
	                                    <Form.Control 
	                                        className="input"
	                                        type="text" 
	                                        placeholder={mobileNo}
	                                        value={mobileNo}
											onChange={(e) => setMobileNo(e.target.value)}
	                                        required
	                                    />
									</Col>

									<Col md="6">
										<Form.Label className="mt-3 text-muted">Email</Form.Label>
	                                    <Form.Control 
	                                        className="input"
	                                        type="text" 
	                                        placeholder={email}
	                                        value={email}
											onChange={(e) => setEmail(e.target.value)}
	                                        required
	                                    />
									</Col>
									<Button type="submit" block variant="success" className="btn-purple my-3"><span className="d-flex align-self-center justify-content-center">Update Profile</span>
						  			</Button>
								</Form.Row>
							</Form>
						</Col>
					</Row>
				</Card.Body>
			</Card>
		</React.Fragment>
	)
}
