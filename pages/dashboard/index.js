import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import Link from 'next/link';
import { Container, Form, Row, Col, Card, Image, Button } from 'react-bootstrap';
import AddCategory from '../../components/AddCategory';
import RecordList from '../../components/RecordList';
import NaviBar from '../../components/NaviBar';
import PieChart from '../../components/PieChart';
import View from '../../components/View';

export default function index() {
    const { user } = useContext(UserContext);
    const [name, setName] = useState('');
    const [categories, setCategories] = useState([]);
    const [records, setRecords] = useState([]);
    const [filteredRecords, setFilteredRecords] = useState([]);
    const [incomeSum, setIncomeSum] = useState([]);
    const [expenseSum, setExpenseSum] = useState([]);
    const [term, setTerm] = useState('');
    const [filter, setFilter] = useState('');
    const [recordOutput, setRecordOutput] = useState([]);
    const [currentBalance, setCurrentBalance] = useState('');

    // fetch user records and categories hook
    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {

            setName(data.firstName)
            setRecords(data.transactions)
            setCategories(data.categories)

            const incomeCalc = data.transactions.reduce((a, b) => {
                return a + (b.categoryType === 'Income' ? b.amount : 0);
            }, 0);
            setIncomeSum(incomeCalc);

            const expensesCalc = data.transactions.reduce((a, b) => {
                return a + (b.categoryType === 'Expense' ? b.amount : 0);
            }, 0);
            setExpenseSum(expensesCalc);

            const total = data.transactions.reduce((a, b) => {
                return a + (b.categoryType === 'Expense' ? (-b.amount) : b.amount);
            }, 0);
            setCurrentBalance(total);

        })
    },[])

    // auto set records
    useEffect(() => {

        setRecordOutput(records.map(record => {

                return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

        }).reverse().splice(0,5))

    }, [records, categories])

    // filter 
    useEffect(() => {

        console.log(records)

        const updatedRecords = records.map(record => {

            if(record.categoryType === filter) {

                return record 

            } else if(filter === "All" || filter === '') {

                return record 

            } else {

                return null

            }

        })

        setFilteredRecords(updatedRecords)

        setRecordOutput(updatedRecords.map(record => {

            if(record !== null){

                if(record.categoryType === filter) {

                    return <RecordList key={record._id}records={record} categories={categories} setRecords={setRecords} />  

                } else if(filter === "All" || filter === '') {

                    return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

                }

            } else {

                return null

            }

        }))

    },[filter])

    // search
    useEffect(() => {

        if (term !== '') {

            console.log(filteredRecords)

            let updatedRecords = filteredRecords.map((record) => {

                //console.log(record.description.toLowerCase().includes(term.toLowerCase()))

                if (record !== null){

                    if (record.description.toLowerCase().includes(term.toLowerCase())){
                    
                        return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

                    } 

                }

                else {

                    return null

                }
            })

            setRecordOutput(updatedRecords);


        } else {

            const updatedRecords = records.map(record => {

                return <RecordList key={record._id} records={record} categories={categories} setRecords={setRecords} /> 

            })

            setRecordOutput(updatedRecords);

        }

    }, [term])

    return (
        <React.Fragment>
            <View title={ 'Dashboard' }>
                <Row>
                    <Col lg={2} className="p-0">
                        <NaviBar />
                    </Col>

                    <Col lg={10} className="content-wrapper">
                        <Container className="my-5">
                            <h1 className="text-white">Hello, {name}!</h1>
                            <Row>
                                <Col xs md="6">
                                    <Card className="card-balance my-4">
                                        <Card.Body>
                                            {(currentBalance.length === 0)
                                            ?   <>
                                                <h5 className="text-white p-0">Your Balance </h5>
                                                <h2 className="text-white">PHP 0</h2>
                                                </>
                                            :   <>
                                                <h5 className="text-white p-0">Your Balance </h5>
                                                <h2 className="text-white">PHP {currentBalance}</h2>
                                                </>
                                            }
                                            
                                            
                                            <h5 className="text-white">Total Income: PHP <span>{incomeSum}</span></h5>
                                            <h5 className="text-white">Total Expenses: PHP <span>{Math.abs(expenseSum)}</span></h5>
                                        </Card.Body>
                                    </Card>

                                    <Card className="card-component card-transaction mt-4">
                                        <Card.Body>
                                            <Card.Title className="d-flex justify-content-between">
                                                <h3 className="text-white">Categories</h3>
                                            </Card.Title>
                                            <div className="d-flex justify-content-center">
                                            {(categories.length === 0)
                                            ?   <h5 className="text-muted text-center">No categories yet</h5>
                                            :   <PieChart records={records} />
                                            }
                                            </div>
                                            
                                        </Card.Body>
                                    </Card>
                                </Col>

                                <Col xs md="6">
                                    <Card className="card-component card-transaction my-4">
                                        <Card.Body>
                                            <Card.Title className="d-flex justify-content-between">
                                                <h3 className="text-white">Transactions</h3>
                                                <Link href="/transactions">
                                                    <a className="text-right text-purple" role="button">See All</a>
                                                </Link>
                                            </Card.Title>
                                            {(records.length === 0)
                                            ?   <>   
                                                    <h5 className="text-muted text-center">No transactions yet</h5>
                                                    <Link href="/transactions">
                                                        <a className="text-purple text-center nav-link mt-2" role="button">+ Add Transaction</a>
                                                    </Link>
                                                </>
                                            :   <div className="transaction-container">
                                                    {recordOutput}
                                                </div>  
                                            }
                                            
                                        </Card.Body>
                                    </Card>
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                
                </Row>
            </View>
 

        </React.Fragment>
    )
}